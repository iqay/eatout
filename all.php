<?php 
function utf8_str_word_count($string, $format = 0, $charlist = null) {
    if ($charlist === null) {
        $regex = '/\\pL[\\pL\\p{Mn}\'-]*/u';
    }
    else {
        $split = array_map('preg_quote', 
                           preg_split('//u',$charlist,-1,PREG_SPLIT_NO_EMPTY));
        $regex = sprintf('/(\\pL|%1$s)([\\pL\\p{Mn}\'-]|%1$s)*/u',
                         implode('|', $split));
    }

    switch ($format) {
        default:
        case 0:
            
            return preg_match_all($regex, $string);

        case 1:
            $results = null;
            preg_match_all($regex, $string, $results);
            return $results[0];
        case 2:
            $results = null;
            preg_match_all($regex, $string, $results, PREG_OFFSET_CAPTURE);
            return empty($results[0])
                ? array()
                : array_combine(
                      array_map('end', $results[0]), 
                      array_map('reset', $results[0]));
    }
}

{ ?> 
<?php
$text = file_get_contents('one.txt', true);

$words = utf8_str_word_count($text, 1); 

$text2 = file_get_contents('two.txt', true);

$words = utf8_str_word_count($text2, 1); 

$text3 = file_get_contents('three.txt', true);

$words = utf8_str_word_count($text3, 1); 



$frequency = array_count_values($words);

arsort($frequency);

echo '<pre>';
print_r($frequency);
echo '</pre>';
?>
<?php } ?>